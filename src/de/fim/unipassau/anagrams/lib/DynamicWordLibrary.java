/* Anagram Game Application */

package de.fim.unipassau.anagrams.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Implementation of the logic for the Anagram Game application.
 */

final class DynamicWordLibrary extends WordLibrary {

	private String dictFilePath = "./GermanNouns.lib";
	private int numberLines = lineCounter(dictFilePath);
	final private String[] WORD_LIST = new String[numberLines];

	public void initializeArr() {
		try {
			FileReader fr = new FileReader(dictFilePath);
			BufferedReader br = new BufferedReader(fr);

			int i = 0;
			String s = br.readLine();
			while (s != null) {
				WORD_LIST[i] = s;
				i++;
				s = br.readLine();
			}
			br.close();
		} catch (IOException e) {

		}
	}

	public String getWord(int idx) {
		return WORD_LIST[idx];
	}

	final static WordLibrary DEFAULT = new DynamicWordLibrary();

	/**
	 * Singleton class.
	 */
	private DynamicWordLibrary() {
	}

	/**
	 * Gets the word at a given index.
	 * 
	 * @param idx
	 *            index of required word
	 * @return word at that index in its natural form
	 */

	/**
	 * Gets the word at a given index in its scrambled form.
	 * 
	 * @param idx
	 *            index of required word
	 * @return word at that index in its scrambled form
	 */
	public String getScrambledWord(int idx) {
		return scramble(WORD_LIST[idx]);
	}

	/**
	 * Gets the number of words in the library.
	 * 
	 * @return the total number of plain/scrambled word pairs in the library
	 */
	public int getSize() {
		return WORD_LIST.length;
	}

	static int lineCounter(String path) {

		int lineCount = 0;

		try {
			Scanner input;
			input = new Scanner(new File(path));
			while (input.hasNextLine()) {
				input.nextLine();

				lineCount++;
			}
			input.close();

		} catch (FileNotFoundException e) {

		}

		return lineCount;
	}

	/**
	 * Checks whether a user's guess for a word at the given index is correct.
	 * 
	 * @param idx
	 *            index of the word guessed
	 * @param userGuess
	 *            the user's guess for the actual word
	 * @return true if the guess was correct; false otherwise
	 */
	public boolean isCorrect(int idx, String userGuess) {
		return userGuess.equals(getWord(idx));
	}

	private String scramble(String clearWord) {

		ArrayList<Character> remainingChars = string2ArrayList(clearWord);

		char[] scrambledArr = new char[clearWord.length()];

		for (int i = 0; i < scrambledArr.length; i++) {
			int rnd = randomElementIndex(remainingChars);
			scrambledArr[i] = remainingChars.get(rnd);
			remainingChars.remove(rnd);
		}
		return charArr2String(scrambledArr);
	}

	int randomElementIndex(ArrayList<Character> l) {
		Random r = new Random();
		return r.nextInt(l.size());
	}

	String charArr2String(char[] cs) {
		String result = "";
		for (char c : cs) {
			result += c;
		}
		return result;
	}

	private ArrayList<Character> string2ArrayList(String clearWord) {
		char[] clearArr = clearWord.toCharArray();
		ArrayList<Character> remainingChars = new ArrayList<Character>();
		for (char c : clearArr) {
			remainingChars.add(c);
		}
		return remainingChars;
	}

}
