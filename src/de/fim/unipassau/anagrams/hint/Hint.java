
package de.fim.unipassau.anagrams.hint;

/**
 *
 * @author ra
 */
public class Hint {

    private Hints currentHint;
    private String clearWord;
    public Hint(String clrWord) {
        clearWord = clrWord;
        int rnd = (int) (Math.random() * Hints.values().length);
        currentHint = Hints.values()[rnd];
    }
    
    public String giveHint() {
        String result = "";

        switch (currentHint) {
            case FIRST_LETTER:
                result = "First letter of word is " + firstLetter(clearWord);
                break;
            case LAST2LETTERS:
                result = "Word ends with " + last2letters(clearWord);
                break;
            case CONTAINS3LETTERSEQ:
                result = "Word contains a 3 letter sequence" + contains3letterSeq(clearWord);
        }
        return result;
    }
    
    private String firstLetter(String clearWord) {
        return clearWord.substring(0, 1);
    }

    private String last2letters(String clearWord) {
        if(clearWord.length() < 2){
            return "at most 2 letters. Come on, thats easy...";
        } else {
            return clearWord.substring(clearWord.length() - 2, clearWord.length());
        }
    }

    private String contains3letterSeq(String clearWord) {
        if(clearWord.length() < 4){
            return " and not much more. Come on, thats easy...";
        } else {
            int rnd = (int) (Math.random() * (clearWord.length() - 3));
            return " " + clearWord.substring(rnd, rnd + 3);
        }
    }

    private enum Hints {
    FIRST_LETTER, LAST2LETTERS, CONTAINS3LETTERSEQ
    }
}
